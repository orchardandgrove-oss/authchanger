#  authchanger

The authchanger repo has moved to [Jamf's GitHub org](https://github.com/jamf/authchanger).

This repo is now in archive mode for historical access.
